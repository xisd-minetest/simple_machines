--------------------------
--  Machines : carve	--	
--------------------------

local MOD_NAME = minetest.get_current_modname()

-- Intllib
local S = simple_machines.intllib

-- Carving machine
-- -- Dig nodes horizontally

simple_machines.machines.h_auto_breaker = {

	-- Descriptions
	name = "h_auto_breaker",
	short_desc = S("Automated Nodebreaker (Horizontal)"),
	-- Support for doc/help modpack
	doc_longdesc = S("When fueled with combustible this machine move forward while digging blocs on its way."),
    doc_help = S("Right-click to open the inventory."),
	
	-- Inventory spec
	formspec = simple_machines.base_formspec,
	 
	-- Textures 
	texture_right = 'simple_machines_carving_side1.png',
	texture_left = 'simple_machines_carving_side1.png'..'^[transformR180',
	texture_top = 'simple_machines_carving_top1.png',
	texture_bottom = 'simple_machines_carving_bottom1.png',
	texture_back = 'simple_machines_carving_back1.png',
	texture_front = 'simple_machines_carving_front1.png',
	icon_inactive = simple_machines.icon_inactive,
	icon_active = simple_machines.icon_active,
	
	-- Action	
	--action = simple_machines_timer_action_one,
	action_param = 'carve',
	action_step = simple_machines.base_step,
	
	-- Crafting
	craft_recipe = {
			{"default:pick_mese", "mesecons_microcontroller:microcontroller0000", "default:steel_ingot"},
			{"pipeworks:nodebreaker_off", "mesecons_movestones:movestone", "default:furnace"},
			{"default:pick_mese", "mesecons:mesecon_off", "default:steel_ingot"}
		},	
	craft_conditions = function ()
		return minetest.get_modpath('default') and minetest.get_modpath('mesecons') and minetest.get_modpath('mesecons_movestones') and minetest.get_modpath('mesecons_microcontroller') and minetest.get_modpath('pipeworks')
	end,
}
