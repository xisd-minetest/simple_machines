--------------------------
--  Machines : drill	--	
--------------------------

local MOD_NAME = minetest.get_current_modname()

-- Intllib
local S = simple_machines.intllib

-- Drilling machine
-- -- Dig nodes vertically

simple_machines.machines.v_auto_breaker = {

	-- Descriptions
	name = "v_auto_breaker",
	short_desc = S("Automated Nodebreaker (Vertical)"),
	short_desc = S("Automated Nodebreaker (Horizontal)"),
	-- Support for doc/help modpack
	doc_longdesc = S("When fueled with combustible this machine move downward while digging blocs on its way."),
    doc_help = S("Right-click to open the inventory."),

	-- Inventory spec
	formspec = simple_machines.base_formspec,
	
	-- Textures 
	texture_left = 'simple_machines_drilling_side1.png',
	texture_right = 'simple_machines_drilling_side1.png',
	texture_top = 'simple_machines_drilling_top1.png',
	texture_bottom = 'simple_machines_drilling_bottom1.png',
	texture_back = 'simple_machines_drilling_back1.png',
	texture_front = 'simple_machines_drilling_front1.png',
	icon_inactive = simple_machines.icon_inactive,
	icon_active = simple_machines.icon_active,
	
	-- Action	
	--action = simple_machines_timer_action,
	action_param = 'drill',
	action_step = simple_machines.base_step,
	
	-- Crafting
	craft_recipe = {
			{"", "", ""},
			{"default:steel_ingot", MOD_NAME..":h_auto_breaker", "default:steel_ingot"},
			{"", "default:steel_ingot", ""}
		},	
	craft_conditions = function ()
		return ( minetest.registered_nodes[MOD_NAME..":carving_machine"] ~= nil ) and minetest.get_modpath('default')
	end,
}
