-- Simple Machines 
--

local MOD_NAME = minetest.get_current_modname()

-- Intllib
local S
if minetest.get_modpath("intllib") then
	S = intllib.Getter()
else
	S = function(s) return s end
end


-- Initialize tables
simple_machines = {}
simple_machines.intllib = S
simple_machines.debug_level = 'action'
simple_machines.machines = {}

--------------
-- Options  --
--------------

-- Particles : set to false to disable particles
simple_machines.use_particles = true
-- Sound : set to false to disable sound
simple_machines.use_sound = true
-- TODO -- Waste : set to false to disable toxic waste generation 
-- ! not implemented yet (meant to be used with an other mod)
-- simple_machines.toxic_waste = true

-- Default values --
--------------------
-- (Most of these values can be overrided by machine definition)

-- Step : time required for the machine to dig/buid and move to the next node
simple_machines.base_step = 5

simple_machines.icon_active = 'simple_machines_active.png'
simple_machines.icon_inactive = 'simple_machines_inactive.png'
simple_machines.base_formspec = "size[8,8.5]"..
								default.gui_bg..
								default.gui_bg_img..
								default.gui_slots..
								"list[current_name;fuel;2.75,2.5;1,1;]"..
								"list[current_player;main;0,4.25;8,1;]"..
								"list[current_player;main;0,5.5;8,3;8]"..
								"listring[current_player;main]"..
								"listring[current_player;main]"..
								default.get_hotbar_bg(0, 4.25)

--------------------------
--  Machines			--	
--------------------------

dofile(minetest.get_modpath(MOD_NAME).."/machine_carve.lua")
dofile(minetest.get_modpath(MOD_NAME).."/machine_drill.lua")
dofile(minetest.get_modpath(MOD_NAME).."/machine_bridge.lua")

--------------------------
--  Common fonctions	--	
--------------------------

--
-- Node callback functions adapted from minetest_game default mod furnace.lua
-- 

local function can_dig(pos, player)
	local meta = minetest.get_meta(pos);
	local inv = meta:get_inventory()
	local timer = minetest.get_node_timer(pos)
	return inv:is_empty("fuel") and inv:is_empty("dst") and not timer:is_started()
end

local function allow_metadata_inventory_put(pos, listname, index, stack, player)
	if minetest.is_protected(pos, player:get_player_name()) then
		return 0
	end
	local meta = minetest.get_meta(pos)
	local inv = meta:get_inventory()
	if listname == "fuel" then
		if minetest.get_craft_result({method="fuel", width=1, items={stack}}).time ~= 0 then
			return stack:get_count()
		else
			return 0
		end
	elseif listname == "dst" then
		-- return 0
		if minetest.is_protected(pos, player:get_player_name()) then
			return 0
		end
		return stack:get_count()
	end
end

local function allow_metadata_inventory_move(pos, from_list, from_index, to_list, to_index, count, player)
	local meta = minetest.get_meta(pos)
	local inv = meta:get_inventory()
	local stack = inv:get_stack(from_list, from_index)
	return allow_metadata_inventory_put(pos, to_list, to_index, stack, player)
end

local function allow_metadata_inventory_take(pos, listname, index, stack, player)
	if minetest.is_protected(pos, player:get_player_name()) then
		return 0
	end
	return stack:get_count()
end

--
-- --
--

local function simple_machines_gravity_test(pos)
	local under = { x = pos.x, y = pos.y - 1, z = pos.z }
	local under_name = minetest.get_node(under).name
	-- If there is air under next pos, next pos in under next pos
	if(under_name ~= "air" and under_name ~= "ignore") then
		return pos
	else
		return simple_machines_gravity_test(under)
	
	end
end

local function simple_machines_effects(pos, timeout)
	if simple_machines.use_sound then
		-- Play sound
		local handle = minetest.sound_play("simple_machines_engine", {
			pos = pos,
			loop = true, 
			max_hear_distance = 10,
			gain = 0.6,
			})
			
		-- Stop sound 
		minetest.after(timeout, function(handle)
				minetest.sound_stop(handle)
			end, handle)
		
		-- TODO : store handle as meta to be stoped on_dig
		-- local meta = minetest.get_meta(pos)
		-- meta:set_float('handle',handle)		
	end
	
	if simple_machines.use_particles then
		-- Add particles
		minetest.add_particlespawner({
			amount = 20,
			time = timeout,
			minpos = { x = pos.x, y = pos.y + 0.1, z = pos.z },
			maxpos = { x = pos.x, y = pos.y + 1, z = pos.z },
			minvel = { x = -0.2, y = -0.1, z = -0.2 },
			maxvel = { x = 0.2, y = 0.1, z = 0.2 },
			minacc = { x = -0.1, y = -0.1, z = -0.1 },
			maxacc = { x = 0.1, y = 0.1, z = 0.1 },
			minexptime = 2,
			maxexptime = 3,
			minsize = 1,
			maxsize = 3,
			collisiondetection = false,
			vertical = true,
			texture = "simple_machines_particles.png",
			-- playername = "singleplayer"
			})	
	end
end

--
-- Action exectuted by machines (called by node timer fonction)
-- 

local function simple_machines_action(pos, new_pos, tab)
	-- Nodes infos
	local node = minetest.get_node(pos)
	local new_pos_node = minetest.get_node(new_pos)
	-- Action parameter (drill, carve, bridge)
	local action_param = tab.action_param	
	
	
--[ Try to dig ]--
	-- If there is something to dig (...and that's what we want to do )
	if(new_pos_node.name ~= "air" and new_pos_node.name ~= "ignore" and action_param ~= 'bridge') then
		-- Try to dig the obstacle
		minetest.dig_node(new_pos)
	end

	-- If node is still there, we can't dig it 
	-- or we don't want to beacause we're building a bridge
	-- or it has not been area has not been loaded yet (ignore) 
	local new_pos_node = minetest.get_node(new_pos)
	if(new_pos_node.name ~= "air") then return end

--[ Fall ]--
	-- If we stand on air, digging machines fall ... 
	if action_param ~= 'bridge' then
		-- While there is air under next pos, next pos in under next pos
		new_pos = simple_machines_gravity_test(new_pos)
	end
				
--[ Move ]--
	-- Save old meta
	local meta = minetest.get_meta(pos)
	local inv = meta:get_inventory()
	local fuel_list = inv:get_list("fuel")
	local dst_list = inv:get_list("dst")
	local fuel_saved_name = meta:get_string('fuel_saved_name')
	local n = meta:get_int('n')
	local i = meta:get_int('i')			
	local bridge_material = meta:get_string('bridge_material')		
	
	-- Remove old node
	minetest.set_node(pos, {name="air"})
	
	-- Replace it with bridge material if we're trying to build a bridge
	if action_param == 'bridge' then
		minetest.set_node(pos, {name=bridge_material})
	end	
				
	-- Swap Node				
	minetest.swap_node(new_pos, node)

	-- Set new meta
		local meta = minetest.get_meta(new_pos)
		local inv = meta:get_inventory()
		local formspec = tab.formspec.."image[2.75,1.5;1,1;"..tab.icon_inactive.."]"
		meta:set_string("formspec", formspec)
		inv:set_size('fuel', 1)
		inv:set_list('fuel', fuel_list)	
		meta:set_string('fuel_saved_name',fuel_saved_name)
		meta:set_int('n',n)
		meta:set_int('i',i)
		if action_param == 'bridge' then 
			inv:set_size('dst', 2)
			inv:set_list('dst', dst_list)
		 end
		-- Update node
		nodeupdate(new_pos)
		-- Start timer
		local timer = minetest.get_node_timer(new_pos)
		timer:start(1.0)

end

--------------------------
--  Nodes registration	--	
--------------------------

--
-- Loops through machines and register them
--

for _,v in pairs(simple_machines.machines) do
	
	--
	-- Register nodes
	-- 
	minetest.register_node(MOD_NAME..":"..v.name,{
		description= v.short_desc,
		_doc_items_longdesc = v.doc_longdesc or nil,
        _doc_items_usagehelp = v.doc_help or "Right-click to open the inventory.",
		drawtype="normal",
		paramtype = "light",
		paramtype2 = "facedir",
		tiles = {
			v.texture_top, v.texture_bottom,
			v.texture_left, v.texture_right,
			v.texture_back, v.texture_front
		},
		is_ground_content = false,
		groups={cracky=2},
		can_dig = can_dig,
		
	--	on_dig = function(pos, node, digger)
	-- 		minetest.node_dig(pos, node, digger)
	-- 		TODO : stop sound on dig
	--	end,
		
		on_construct = function(pos)
			-- Get node and meta infos
			local node = minetest.get_node(pos)
			local meta = minetest.get_meta(pos)

			-- Init. inventory and 
			local inv = meta:get_inventory()
			inv:set_size('fuel', 1)
			if v.action_param == 'bridge' then inv:set_size('dst', 2) end
			meta:set_string('fuel_saved_name','')
			meta:set_int('n',-1)
			meta:set_int('i',-1)					
			
			-- Init. formspec
			local formspec = v.formspec.."image[2.75,1.5;1,1;"..v.icon_inactive.."]"
			meta:set_string("formspec", formspec)
			
			-- Start timer
			local timer = minetest.get_node_timer(pos)
			if not timer:is_started() then timer:start(1.0)	end
		end,
		
		on_receive_fields = function(pos, formname, fields, sender)
			print("Player "..sender:get_player_name().." submitted fields "..dump(fields))
			-- Start timer
			local timer = minetest.get_node_timer(pos)
			if not timer:is_started() then timer:start(1.0)	
			print("timer started")
			end
		end,
		
		on_timer = function(pos, elapsed)

		-- [ Collect Variables ] --

			-- Node infos
			local node = minetest.get_node(pos)
			-- Meta infos
			local meta = minetest.get_meta(pos)
			-- Inventory
			local inv = meta:get_inventory()
			-- Build materials
			local dst_list = inv:get_list("dst")
			-- Fuel inventory list
			local fuel_list = inv:get_list("fuel")
			local fuel, afterfuel = minetest.get_craft_result({method = "fuel", width = 1, items = fuel_list})
			
			-- Action parameter (drill, carve, bridge)
			local action_param = v.action_param	
			-- Step (interval between digs)
			local step = v.action_step	
				

		--[ Check fuel ]--
			
			-- No valid fuel : retrun
			if fuel.time == 0 then 
				minetest.log(simple_machines.debug_level,'[simple_machines] '..S('machine at')..' '..minetest.pos_to_string(pos)..' '..S('is out of fuel'))
				return
			end

			-- No build material : return
			-- Becaunse inv list is empty
			if action_param == 'bridge' and inv:is_empty("dst") then 
				minetest.log(simple_machines.debug_level,'[simple_machines] '..S('machine at')..' '..minetest.pos_to_string(pos)..' '..S('is out of build materials'))
				return
			-- Or because items it contains are not nodes 	
			elseif action_param == 'bridge' and dst_list[1]:get_definition().type ~= 'node' and dst_list[2]:get_definition().type ~= 'node' then
				minetest.log(simple_machines.debug_level,'[simple_machines] '..S('machine at')..' '..minetest.pos_to_string(pos)..' '..S('is out of valid build materials'))
				return
			end 
			
-- [[		-- I we get there, we have valid fuel...
			local n = meta:get_int('n')
			local i = meta:get_int('i')
			local fuel_saved_name = meta:get_string('fuel_saved_name')
			local fuel_name = fuel_list[1]:get_name() 
			
			-- There is no burning in progress 
			if n < 1 or fuel_name ~= fuel_saved_name then
				--[ Define burn efficiency ]--
				-- Initialize values
				n = 1 	-- number of moves before consumming item
				i = 1	-- number of item consumed		
				
				-- set number of move before the item is consumed
				-- or number of item to be consumed in one move
				-- depending on burning time on fuel and the value of step option
				if fuel.time >= step then
					n = math.floor(fuel.time / step)
					n = math.ceil(n / 2)
				elseif fuel.time < step then
					i = math.floor(step / fuel.time)
				end 
				-- Inform log
				minetest.log(simple_machines.debug_level,'[simple_machines] '..S('machine at')..' '..minetest.pos_to_string(pos)..' '.. string.format(S("will consume %d of %d '%s' to move %d times"),i,fuel_list[1]:get_count(),fuel_list[1]:get_name(),n))
			end
			
			-- Item has reach its burning time... 
			if n == 1 then
				-- Not enought fuel : return
				if not inv:contains_item("fuel", fuel_list[1]:get_name()..' '..i) then 
					minetest.log(simple_machines.debug_level,'[simple_machines] '..S('machine at')..' '..minetest.pos_to_string(pos)..' '..string.format(S("needs at least %d '%s' to move %d times"),i,fuel_list[1]:get_name(),n))
					return
				end

				-- Remove item from fuel inventory
				minetest.log(simple_machines.debug_level,'[simple_machines] '..string.format(S("removing %d '%s' form fuel inventory of machine at %s"),i,fuel_list[1]:get_name(),minetest.pos_to_string(pos)))
				inv:remove_item("fuel", fuel_list[1]:get_name()..' '..i)
				
				-- If there is an afterfuel item put it in the list
				-- (Is there stackable items that have aterfuel ?)
				-- (will it drop is there is still fuel in it  ?)
				-- room_for_item("listname", stack)
				inv:set_stack("fuel", 1, afterfuel.items[1])
			end
			
			-- Take bridge material from inventory	
			if action_param == 'bridge' then
				local bridge_material = 'air' -- safety
				if dst_list[1]:get_definition().type == 'node' and not dst_list[1]:is_empty() then
					bridge_material = dst_list[1]:get_name()
					inv:remove_item("dst", bridge_material)
				elseif dst_list[2]:get_definition().type == 'node' and not dst_list[2]:is_empty() then
					bridge_material = dst_list[2]:get_name()
					inv:remove_item("dst", bridge_material)
				end
				-- Do not place it yet, but save as meta
				meta:set_string('bridge_material', bridge_material)
			end

			
			-- Update meta
			local m =	n - 1
			meta:set_int('n',m)
			meta:set_int('i',i)
			meta:set_string('fuel_saved_name', fuel_name)
			minetest.log(simple_machines.debug_level,'[simple_machines] '..S('moves left before consuming next fuel item')..' : '..m)
		--]]

			
		-- [ Get new pos infos ] --	
			-- Define next pos
			local new_pos
			if(action_param == 'carve' or action_param == 'bridge')then
				new_pos = vector.subtract(pos, minetest.facedir_to_dir(node.param2))
			else -- if action_param == 'drill' then
				new_pos = { x = pos.x, y = pos.y - 1, z = pos.z }
			end	
			
			-- What's there ?
			local new_pos_node = minetest.get_node(new_pos)
						
			----------------------------------------------------
			-- By now we now we have fuel, we can move or dig 
			----------------------------------------------------
			minetest.log(simple_machines.debug_level,'[simple_machines] '..S('machine at')..' '..minetest.pos_to_string(pos)..' '..S('is starting'))


		-- [ Active icon ] --
			-- Show active formspec icon
			local formspec = v.formspec.."image[2.75,1.5;1,1;"..v.icon_active.."]"
			meta:set_string("formspec", formspec)
			-- Switch back to inactive after a while
			minetest.after(step, function(pos, formspec, icon)
				local meta = minetest.get_meta(pos)
				local formspec = formspec.."image[2.75,1.5;1,1;"..icon.."]"
				meta:set_string("formspec", formspec)
			end, pos, v.formspec, v.icon_inactive)	
	
		-- [ Sound and particle effects ] --
			simple_machines_effects(pos, step)	

		--[ Execute actual action after some time ]-- 
			-- digging on building bridge takes time
			minetest.log(simple_machines.debug_level,'[simple_machines] '..S('machine at')..' '..minetest.pos_to_string(pos)..' '..string.format(S("is active for the next %d seconds"),step))

			minetest.after(step, simple_machines_action, pos, new_pos, v)

		end,
		
	})

	--
	-- Craft recipe
	-- 
	if v.craft_conditions then
		minetest.register_craft({
			output = MOD_NAME..":"..v.name,
			recipe = v.craft_recipe
		
		})
	end
		
end


