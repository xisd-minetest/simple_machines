--------------------------
--  Machines : bridge	--	
--------------------------

local MOD_NAME = minetest.get_current_modname()

-- Intllib
local S = simple_machines.intllib

-- Bridge machine
-- -- Create bridge

simple_machines.machines.h_auto_deployer = {

	-- Descriptions
	name = "h_auto_deployer",
	short_desc = S("Automated Deployer"),
	-- Support for doc/help modpack
	doc_longdesc = S("When fueled with combustible this machine move forward while placing blocs contained in its inventory."),
    doc_help = S("Right-click to open the inventory."),

	-- Inventory spec
	formspec = simple_machines.base_formspec
				.."list[current_name;dst;4.75,2.96;2,1;]"
				.."listring[current_name;dst]",
	
	-- Textures 
	texture_left = 'simple_machines_bridge_side1.png',
	texture_right = 'simple_machines_bridge_side1.png'..'^[transformR180',
	texture_top = 'simple_machines_bridge_top1.png',
	texture_bottom = 'simple_machines_bridge_bottom1.png',
	texture_back = 'simple_machines_bridge_back1.png',
	texture_front = 'simple_machines_bridge_front1.png',
	icon_inactive = simple_machines.icon_inactive,
	icon_active = simple_machines.icon_active,
	
	-- Action	
	--action = simple_machines_timer_action_one,
	action_param = 'bridge',
	action_step = simple_machines.base_step,
	
	-- Crafting
	craft_recipe = {
			{ "default:steel_ingot", "mesecons_microcontroller:microcontroller0000", "default:wood"},
			{"default:furnace", "mesecons_movestones:movestone", "pipeworks:deployer_off"},
			{"default:steel_ingot", "mesecons:mesecon_off", "default:wood"}
		},	
	craft_conditions = function ()
		return minetest.get_modpath('default') and minetest.get_modpath('mesecons') and minetest.get_modpath('mesecons_movestones') and minetest.get_modpath('mesecons_microcontroller') and minetest.get_modpath('pipeworks')
	end,
}

