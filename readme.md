## Simple Machines ##
---------------------

Licenses: code LGPL 2.1 media CC BY-SA 3.0
See licences.txt for details

---------------
### Content ###
---------------

Adds 3 fuel powered machines :
- Carving 	: dig nodes horizontally
- Drilling	: dig nodes horizontally
- Bridge	: build a bridge

-------------------
### Screenshots ###
-------------------

![Machines](https://github.com/xisd/simple_machines/raw/master/screenshot_1.png)
![Recipes](https://github.com/xisd/simple_machines/raw/master/screenshot_2.png)

---------------
### Recipes ###
---------------
(Recipes are not registered if any of the required materials is not present)

* Carving machine : automated nodebreaker (horizontal)
- - - - - - - - - - - - - - - - - - - - - - - - 
 mese pick 		| microcontroller | steel ingot	
 node breaker	|   movestone	  | furnace		
 mese pick		|   mesecon 	  | steel ingot	
- - - - - - - - - - - - - - - - - - - - - - - - 

* Drilling machine : automated nodebreaker (vertical)
- - - - - - - - - - - - - - - - - - - - - - - - 
 				|					|				
 steel ingot	|   carving machine	| steel ingot	
 				|   steel ingot	 	| 				
- - - - - - - - - - - - - - - - - - - - - - - - 

* Bridge machine : automated deployer
- - - - - - - - - - - - - - - - - - - - - - - - 
 steel ingot	| microcontroller | wood		
 furnace 		|   movestone	  | deployer 	
 steel ingot	|   mesecon 	  | wood		
- - - - - - - - - - - - - - - - - - - - - - - - 


----------------------
### Future things ###
----------------------

- [x] Support for intlib  
- [ ] French Translation  
- [ ] Support for help modpack 
- [ ] Support for awards  
- [ ] Alternatives recipes

- [ ] Possible future machines
    - Vertical 'bridges' 
		-- can be used to place ladders or other things)
	- Vaccum pipe bridge machine 
		-- build pipe (from pipeworks) and vaccum dropped items into it 
	- Stairs machine 
		-- alternate actions (drill, carve)



-------------------
#### Side note ####
-------------------

>> I may also make a mod to work in combination with it
>> Something a bit like pollution by UjEdwin but more focused on waste disposal...  
>> https://forum.minetest.net/viewtopic.php?f=9&t=13506 
>> 
>> it would encourage players to work together to prevent spreading of pollution
>> and find ways to dispose of toxic waste
>> 
>> 
>> ex : machines randomly converts one of surounding nodes to toxic waste
>>>  (exept a few materials (glass, obsidian, ?) )
>>>  toxic stuff slowly infects surrounding blocs
>>>  it can only be dug with a one use special tool
>>>  or some kind on containement container
>>>  or/and it must be carried trought pipes
>>>  or it can be dug but cannot be stored in chest or other node containers
>>>  maybe if player keep it to long in inventory it attracts zombies
>>>  this would encourage creation of disposal areas surronded by some immunes materials
>>>  but maybe that is not all...
>>>  maybe monsters spawns on it so it must be well contained
>>>  depending on quantity of the stuff in the area there may also be some node breaking monsters
>>>  those would create the need to manage size of dumping area as well a some maintenance and precautions
